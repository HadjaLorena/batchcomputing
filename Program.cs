﻿using System.Diagnostics;

static void Exec(int PID, double time)
{
    Console.WriteLine($"Processo {PID} está sendo executado...");
    Thread.Sleep((int)(time * 1000));
    Console.WriteLine($"Processo {PID} foi finalizado.");
}

var watch = new Stopwatch();
var random = new Random();

watch.Start();

for (int i = 0; i < 4; i++)
{
    int PID = random.Next(1000, 5000);
    int time = (i + 1) * 4 / 2;

    Exec(PID, time);
}

watch.Stop();

Console.WriteLine($"Tempo total de execução: {Math.Round(watch.Elapsed.TotalSeconds)}s");

watch.Restart();

static void RunAsyncProcesses()
{
    var process1 = new Thread(() => Exec(1234, 6));
    process1.Start();

    var process2 = new Thread(() => Exec(9867, 3));
    process2.Start();

    var process3 = new Thread(() => Exec(7564, 2));
    process3.Start();

    var process4 = new Thread(() => Exec(1456, 8));
    process4.Start();

    process1.Join();
    process2.Join();
    process3.Join();
    process4.Join();
}

watch.Start();
RunAsyncProcesses();
watch.Stop();

Console.WriteLine($"Tempo total de execução: {Math.Round(watch.Elapsed.TotalSeconds)}s");